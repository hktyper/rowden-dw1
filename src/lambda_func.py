import boto3
import os

def lambda_handler(event, context):
    # Initialize Boto3 client for EC2 interaction
    ec2 = boto3.client('ec2')

    # Specify the string to search for in instance names
    target_string = os.environ.get('target_string', 'Rowden')  # 'Rowden' as default 

    # Get all running instances 
    filters = [
        {
            'Name': 'instance-state-name', 
            'Values': ['running']
        }
    ]
    instances = ec2.describe_instances(Filters=filters)

    # Gather IDs of instances matching the target string
    instance_ids = []
    for reservation in instances['Reservations']:
        for instance in reservation['Instances']:
            if 'Tags' in instance:
                for tag in instance['Tags']:
                    if tag['Key'] == 'Name' and target_string in tag['Value']:
                        instance_ids.append(instance['InstanceId'])

    # Stop the matching instances
    if instance_ids:
        ec2.stop_instances(InstanceIds=instance_ids)
        print('Stopping instances: ' + str(instance_ids))
    else:
        print('No instances found matching the target string.')
