# Rowden ex

## Getting started

Pre-requisites

Configure secrets and variables in Settings

```
AWS_ACCESS_KEY_ID
AWS_ACCOUNT_ID
AWS_SECRET_ACCESS_KEY
```


## Gotchas and FYI

When developing on MacBooks runinng Mx chips, include the following statements in your docker build commands

```
--platform linux/amd64
```

## Integrate with your tools

- [ ] [Terraform Docs](https://github.com/terraform-docs/terraform-docs)

***

## Other considerations
Depending on number of EC2's and also number of physical accounts within an Org, may be worth exploring.

https://aws.amazon.com/es/solutions/implementations/instance-scheduler-on-aws/


## Description
1. Python code for a lambda that will turn off any EC2 instances that match a particular string (for
example any instances with the word “Rowden” in them).
2. Terraform code to launch that lambda with any appropriate security group and IAM settings, in
AWS.

## Architecture


## License


<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 5.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_archive"></a> [archive](#provider\_archive) | 2.4.2 |
| <a name="provider_aws"></a> [aws](#provider\_aws) | 5.42.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_iam_role.lambda_ec2_stopper](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy.lambda_ec2_stopper_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | resource |
| [aws_lambda_function.instance_stopper](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_function) | resource |
| [archive_file.lambda_zip](https://registry.terraform.io/providers/hashicorp/archive/latest/docs/data-sources/file) | data source |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_target_string"></a> [target\_string](#input\_target\_string) | Target string to search for when running lambda function to stop EC2 instances | `string` | `"Rowden"` | no |

## Outputs

No outputs.
<!-- END_TF_DOCS -->