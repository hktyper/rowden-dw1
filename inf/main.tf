data "archive_file" "lambda_zip" {
  type        = "zip"
  source_dir  = "../src" # Directory with your code  
  output_path = "../src/lambda.zip" 
}

resource "aws_lambda_function" "instance_stopper" {
  filename         = "../src/lambda.zip" # Path to the lambda function
  function_name    = "instance_stopper"
  role             = aws_iam_role.lambda_ec2_stopper.arn # Create a suitable IAM role
  handler          = "lambda_func.lambda_handler"
  runtime          = "python3.9" # Adjust as needed

  # Pass the target string as an environment variable 
  environment {
    variables = {
      target_string = var.target_string            
    }
  }
}