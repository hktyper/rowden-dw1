variable "target_string" {
  description = "Target string to search for when running lambda function to stop EC2 instances"
  type = string 
  default = "Rowden"
}